# FormDev

## Démarrer sur le projet

Installer les dépendances npm:
```
npm install
```

Utiliser la bonne version de Node.js (recommandé 16.10).
Il est possible d'utiliser automatiquement la bonne version à l'aide de [NVM](https://github.com/nvm-sh/nvm):
```
nvm use
```

Lancer le projet:
```
npm run start
```

## Construire et lancer l'application en mode production

> Note: Le ServiceWorker utilisé pour la PWA n'est utilisable qu'en exécution en production.

```
npm run build
npm run server
```
