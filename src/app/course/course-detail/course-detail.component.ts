import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { combineLatest, map, Observable, startWith, switchMap } from "rxjs";
import { Store, Select } from "@ngxs/store";
import { actionsExecuting } from "@ngxs-labs/actions-executing";

import { Course } from "src/app/data/models/course.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { CoursesState } from "src/app/data/states/courses.state";
import { CloseCourse, GetCourse } from "src/app/data/actions/courses.actions";
import { Signature } from "src/app/data/models/signature.model";
import { UpdateSignature } from "src/app/data/actions/signatures.actions";
import { AuthenticationService } from "src/app/security/authentication.service";
import { PendingSignature, SignaturesState } from "src/app/data/states/signatures.state";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CourseService } from "src/app/data/services/course.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-course-detail",
  templateUrl: "./course-detail.component.html",
  styleUrls: ["./course-detail.component.scss"],
})
export class CourseDetailComponent implements OnInit {
  course$?: Observable<StrapiData<Course> | undefined>;
  signaturesWaitingSync$?: Observable<PendingSignature[]>;

  displayModal = false;

  absModalDisplay = false;
  absMsg  = "";

  displayModalComponent = false;

  selectedSignature?: StrapiData<Signature>;

  commentForm: FormGroup;

  @Select(actionsExecuting([GetCourse])) loadingCourse$!: Observable<boolean>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly authenticationService: AuthenticationService,
    private readonly store: Store,
    private readonly courseService: CourseService,
    private readonly onlineStatusService: OnlineStatusService,
    private readonly messageService: MessageService,
    fb: FormBuilder
  ) {
    this.commentForm = fb.group({
      comment: ["", [Validators.required]],
    });
  }

  logout() {
    this.authenticationService.logout();
  }

  ngOnInit() {
    const rawId = this.route.snapshot.paramMap.get("id");

    if (rawId) {
      const id = Number.parseInt(rawId, 10);

      if (id !== NaN) {
        // Prepare observable.
        this.course$ = this.store
          .select(CoursesState.courses)
          .pipe(map((courses) => courses.find((course) => course.id === id)));

        // Trigger fetch course in case it's unknown.
        this.store.dispatch(new GetCourse(id));

        this.signaturesWaitingSync$ = combineLatest([this.store.select(SignaturesState.signatures), this.course$])
          .pipe(
            map(([signatures, course]) => signatures?.filter(s => s.courseId === course?.id) || [])
          );

        return;
      }
    }

    this.goToList();
  }

  setAsAbsent(courseId: number, signature: StrapiData<Signature>) {
    this.selectedSignature = signature;
    this.onAbsent(courseId);
  }

  onAddComment(courseId: number) {
    const comment = this.commentForm.get('comment')?.value;

    if (comment) {
      this.courseService.addComment(courseId, comment).pipe(
        switchMap(() => this.store.dispatch(new GetCourse(courseId)))
      ).subscribe(() => {
        this.messageService.add({ severity: 'success', summary: 'Commentaire ajouté.'});
        this.commentForm.reset();
      });
    }
  }

  onDeleteComment(courseId: number, commentId: number) {
    this.courseService.removeComment(commentId).pipe(
      switchMap(() => this.store.dispatch(new GetCourse(courseId)))
    ).subscribe(() => {
      this.messageService.add({ severity: 'error', summary: 'Commentaire supprimé.'});
    });
  }

  onPresent(courseId: number, signature: string) {
    if (this.selectedSignature) {
      this.store.dispatch(
        new UpdateSignature(courseId, {
          id: this.selectedSignature.id,
          attributes: {
            present: true,
            date: new Date(),
            signature
          }
        })
      ).subscribe(() => this.store.dispatch(new GetCourse(courseId)));
    }

    this.closeSignature();
  }

  onAbsent(courseId: number) {
    if (this.selectedSignature) {
      this.store.dispatch(
        new UpdateSignature(courseId, {
          id: this.selectedSignature.id,
          attributes: {
            present: false,
            date: new Date()
          }
        })
      ).subscribe(() => this.store.dispatch(new GetCourse(courseId)));
    }

    this.closeSignature();
  }

  onClose(courseId: number, signature: string) {
    this.store.dispatch(
      new CloseCourse(courseId, signature, new Date())
    );
  }

  openSignature(signature: StrapiData<Signature>) {
    this.selectedSignature = signature;
    this.displayModal = true;
  }

  closeSignature() {
    this.selectedSignature = undefined;
    this.displayModal = false;
  }

  get selectedAttendeeName() {
    const attendee = this.selectedSignature?.attributes?.attendee?.data?.attributes;
    return `${attendee?.firstname} ${attendee?.lastname.toUpperCase()}`;
  }

  goToList() {
    this.router.navigate(["/courses"]);
  }
  isOnline() {
    return this.onlineStatusService.status.pipe(
      startWith(this.onlineStatusService.getStatus()),
      map((status) => status === OnlineStatusType.ONLINE)
    );
  }

  sendEmailToAbs(courseId: number) {
    this.courseService.sendEmail(courseId).pipe(
      switchMap(() => this.store.dispatch(new GetCourse(courseId)))
    ).subscribe({
      next: (v) => {
        console.log(v);
        this.absModalDisplay = true;
        this.absMsg = "Email envoyé avec succès.";
      },
      error: (e) => {
        this.absModalDisplay = true;
        this.absMsg = "Une erreur est survenu lors de l'envoi de l'email.";
      },
      complete: () => console.info('complete')
    });
  }

  sendEmailBySignature(signatureId: number, courseId: number) {
    this.courseService.sendEmailBySignature(signatureId).pipe(
      switchMap(() => this.store.dispatch(new GetCourse(courseId)))
    ).subscribe({
      next: (v) => {
        console.log(v);
        this.absModalDisplay = true;
        this.absMsg = "Email envoyé avec succès.";
      },
      error: (e) => {
        this.absModalDisplay = true;
        this.absMsg = "Une erreur est survenu lors de l'envoi de l'email.";
      },
      complete: () => console.info('complete')
    });
  }
}
