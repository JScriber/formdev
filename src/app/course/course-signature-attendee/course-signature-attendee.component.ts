import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { map, Observable, startWith } from "rxjs";
import { Store, Select } from "@ngxs/store";
import { actionsExecuting } from "@ngxs-labs/actions-executing";

import { Course } from "src/app/data/models/course.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { GetCourse } from "src/app/data/actions/courses.actions";
import { CoursesState } from "src/app/data/states/courses.state";
import { Signature } from "src/app/data/models/signature.model";

import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import { UpdateSignature } from "src/app/data/actions/signatures.actions";
import { Attendee } from "src/app/data/models/attendee.model";

@Component({
  selector: "app-course-signature-attendee",
  templateUrl: "./course-signature-attendee.component.html",
  styleUrls: ["./course-signature-attendee.component.scss"],
})
export class CourseSignatureAttendeeComponent implements OnInit {
  course$?: Observable<StrapiData<Course> | undefined>;
  signature$?: Observable<StrapiData<Signature> | undefined>;
  attendee$?: Observable<StrapiData<Attendee> | undefined>;

  @Select(actionsExecuting([GetCourse])) loadingCourse$!: Observable<boolean>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly store: Store,
    private readonly onlineStatusService: OnlineStatusService
  ) {}

  ngOnInit() {
    registerLocaleData(localeFr, "fr");
    const key = this.route.snapshot.paramMap.get("key");
    const keySignature = this.route.snapshot.paramMap.get("keySignature");

    if (key) {
      // Prepare observable.
      this.course$ = this.store
        .select(CoursesState.courses)
        .pipe(
          map((courses) =>
            courses.find((course) => course.attributes.key === key)
          )
        );

      this.signature$ = this.course$.pipe(
        map((course) =>
          course?.attributes.signatures.data.find(
            (signature) => signature.attributes.key === keySignature
          )
        )
      );

      this.attendee$ = this.signature$.pipe(
        map((signature) => signature?.attributes.attendee.data)
      );

      // Trigger fetch course in case it's unknown.
      this.store.dispatch(new GetCourse(key));
      return;
    }
  }

  onPresent(courseId: number, signatureId: number, signature: string) {
    if (signatureId) {
      this.store
        .dispatch(
          new UpdateSignature(courseId, {
            id: signatureId,
            attributes: {
              present: true,
              date: new Date(),
              signature,
            },
          })
        )
        .subscribe(() => this.store.dispatch(new GetCourse(courseId)));
    }
  }

  isOnline() {
    return this.onlineStatusService.status.pipe(
      startWith(this.onlineStatusService.getStatus()),
      map((status) => status === OnlineStatusType.ONLINE)
    );
  }

  isDateEnded(signature: StrapiData<Signature>) {
    const dateSignatureMax = new Date(signature.attributes.date_limit);
    const today = new Date();
    if (signature.attributes.date_limit && today > dateSignatureMax) {
      return true;
    } else {
      return false;
    }
  }
}
