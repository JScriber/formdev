import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { Store, Select } from "@ngxs/store";
import { actionsExecuting } from "@ngxs-labs/actions-executing";

import { Course } from "src/app/data/models/course.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { GetCourses } from "src/app/data/actions/courses.actions";
import { CoursesState } from "src/app/data/states/courses.state";
import { AuthenticationService } from "src/app/security/authentication.service";
import * as dayjs from "dayjs";
import { CalendarOptions } from "@fullcalendar/core";
import frLocale from "@fullcalendar/core/locales/fr";

@Component({
  selector: "app-course-list",
  templateUrl: "./course-list.component.html",
  styleUrls: ["./course-list.component.scss"],
})
export class CourseListComponent implements OnInit {
  @Select(CoursesState.courses) courses$!: Observable<StrapiData<Course>[]>;

  @Select(actionsExecuting([GetCourses])) loadingCourses$!: Observable<boolean>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly store: Store,
    private readonly authenticationService: AuthenticationService
  ) {}

  logout() {
    this.authenticationService.logout();
  }

  openCourse(course: StrapiData<Course>) {
    this.router.navigate([course.id], { relativeTo: this.route });
  }

  header: any;

  options$ = this.store.select(CoursesState.courses).pipe(
    map(
      (courses) =>
        ({
          initialDate: dayjs().format("YYYY-MM-DD"),
          locale: frLocale,
          allDaySlot: false,
          headerToolbar: {
            left: "prev,next today",
            center: "title",
            right: "timeGridDay,timeGridWeek,dayGridMonth",
          },
          initialView: "timeGridDay",
          events: courses.map((course) => ({
            id: course.id,
            title: course.attributes.title,
            start: dayjs(course.attributes.start).format("YYYY-MM-DD HH:mm"),
            end: dayjs(course.attributes.end).format("YYYY-MM-DD HH:mm"),
          })),
          eventClick: (info) => {
            this.router.navigate([info.event.id], { relativeTo: this.route });
          },
        } as CalendarOptions)
    )
  );

  ngOnInit(): void {
    setTimeout(function () {
      window.dispatchEvent(new Event("resize"));
    }, 1);
    this.store.dispatch(new GetCourses());
  }
}
