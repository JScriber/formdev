import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Course } from 'src/app/data/models/course.model';

@Component({
  selector: 'app-course-card-link',
  templateUrl: './course-card-link.component.html',
  styleUrls: ['./course-card-link.component.scss']
})
export class CourseCardLinkComponent {

  @Input() course!: Course;

  @Output() open = new EventEmitter<void>();

}
