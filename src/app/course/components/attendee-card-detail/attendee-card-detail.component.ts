import {
  Component,
  Input,
  OnChanges,
  OnInit,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from "@ngxs/store";
import { switchMap } from "rxjs";
import { GetCourse } from "src/app/data/actions/courses.actions";
import { Attendee } from "src/app/data/models/attendee.model";
// import { Signature } from "src/app/data/models/signature.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { StrapiResponse } from "src/app/data/models/strapi/strapi-response";
import { CourseService } from "src/app/data/services/course.service";

@Component({
  selector: "app-attendee-card-detail",
  templateUrl: "./attendee-card-detail.component.html",
  styleUrls: ["./attendee-card-detail.component.scss"],
})
export class AttendeeCardDetailComponent implements OnInit, OnChanges {
  @Input() courseId!: number | string;

  @Input() attendee!: StrapiResponse<StrapiData<Attendee>, undefined>;

  form: FormGroup;
  commentForm: FormGroup;
  formBlocked: boolean = true;

  constructor(private readonly courseService: CourseService,
              private readonly store: Store,
              fb: FormBuilder) {
    this.form = fb.group({
      firstname: ["", [Validators.required]],
      lastname: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      // psw: ["", [Validators.required]],
    });
    this.commentForm = fb.group({
      comment: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.form.patchValue({
      ...this.attendee.data.attributes,
      // date: new Date(this.signature.date),
    });
    this.form.disable();
  }

  ngOnChanges(): void {
    console.log(this.attendee);
    if (this.attendee.data.attributes.firstname !== this.form.value.firstname) {
      this.form.patchValue({
        ...this.attendee.data.attributes,
        // date: new Date(this.signature.date),
      });
    }
  }

  toggleForm(reset = false) {
    this.formBlocked = !this.formBlocked;
    if (this.formBlocked) {
      this.form.disable();
      if (reset) {
        this.form.patchValue({
          ...this.attendee.data.attributes,
          // date: new Date(this.signature.date),
        });
      }
    } else {
      this.form.enable();
    }
  }

  submit() {
    if (this.form.valid) {
      this.courseService.updateAttendee(this.attendee.data.id, this.form.value).pipe(
        switchMap(() => this.store.dispatch(new GetCourse(this.courseId)))
      ).subscribe(() => {});
    }
  }

  onAddComment() {
    const comment = this.commentForm.get('comment')?.value;

    if (comment) {
      this.courseService.addAttendeeComment(this.courseId, this.attendee.data.id, comment).pipe(
        switchMap(() => this.store.dispatch(new GetCourse(this.courseId)))
      ).subscribe(() => {
        this.commentForm.reset();
      });
    }
  }

  onDeleteComment(commentId: number) {
    this.courseService.removeAttendeeComment(commentId).pipe(
      switchMap(() => this.store.dispatch(new GetCourse(this.courseId)))
    ).subscribe(() => {});
  }
}
