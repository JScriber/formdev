import { Component, EventEmitter, Input, Output } from "@angular/core";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { map, startWith } from "rxjs";
import { GetCourse } from "src/app/data/actions/courses.actions";

import { Attendee } from "src/app/data/models/attendee.model";
import { Course } from "src/app/data/models/course.model";
import { Signature } from "src/app/data/models/signature.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { StrapiResponse } from "src/app/data/models/strapi/strapi-response";
import { CourseService } from "src/app/data/services/course.service";
import { PendingSignature } from "src/app/data/states/signatures.state";

@Component({
  selector: "app-course-card-attendee-list",
  templateUrl: "./course-card-attendee-list.component.html",
  styleUrls: ["./course-card-attendee-list.component.scss"],
})
export class CourseCardAttendeeListComponent {
  @Input() course!: StrapiData<Course>;
  @Input() speaker!: boolean;
  @Input() qrcode: boolean = false;
  @Input() pendingSignatures!: PendingSignature[] | null;

  @Output() openSignature = new EventEmitter<StrapiData<Signature>>();

  @Output() absent = new EventEmitter<StrapiData<Signature>>();

  constructor(private readonly onlineStatusService: OnlineStatusService, private readonly courseService: CourseService) {}

  showEditDialog: boolean = false;
  personnalEmailDisplay: boolean = false;
  personnalEmailMessage: string = "";
  selectedAttendee?: StrapiResponse<StrapiData<Attendee>, undefined> =
    undefined;

  toggleEditDialog(signature: any) {
    this.showEditDialog = !this.showEditDialog;
    this.selectedAttendee = signature.attributes.attendee;
  }

  isWaitingForSync(signature: StrapiData<Signature>): boolean {
    return (
      this.pendingSignatures?.find((s) => s.signature.id === signature.id) !==
      undefined
    );
  }

  isOnline() {
    return this.onlineStatusService.status.pipe(
      startWith(this.onlineStatusService.getStatus()),
      map((status) => status === OnlineStatusType.ONLINE)
    );
  }

  isDateEnded(signature: StrapiData<Signature>) {
    const dateSignatureMax = new Date(signature.attributes.date_limit);
    const today = new Date();
    if (signature.attributes.date_limit && today > dateSignatureMax) {
      return true;
    } else {
      return false;
    }
  }

  sendSignatureEmail(signatureId: number) {
    this.courseService.sendEmailBySignature(signatureId).subscribe({
      next: (v) => {
        console.log(v);
        this.personnalEmailDisplay = true;
        this.personnalEmailMessage = "Email envoyé avec succès.";
      },
      error: (e) => {
        this.personnalEmailDisplay = true;
        this.personnalEmailMessage = "Une erreur est survenu lors de l'envoi de l'email.";
      },
      complete: () => console.info('complete')
    });
  }
}
