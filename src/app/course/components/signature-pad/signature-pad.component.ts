import { Component, ElementRef, Input, AfterViewInit, ViewChild } from '@angular/core';
import SignaturePad, { Options } from 'signature_pad';

@Component({
  selector: 'app-signature-pad',
  templateUrl: './signature-pad.component.html',
  styleUrls: ['./signature-pad.component.scss']
})
export class SignaturePadComponent implements AfterViewInit {

  @Input() options: Options = {};

  signaturePad?: SignaturePad;

  @ViewChild('signaturePadCanvas')
  private canvas!: ElementRef<HTMLCanvasElement>;

  ngAfterViewInit() {
    this.signaturePad = new SignaturePad(this.canvas.nativeElement, this.options);
  }
}
