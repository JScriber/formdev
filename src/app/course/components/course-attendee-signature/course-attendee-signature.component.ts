import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import { SignaturePadComponent } from "../signature-pad/signature-pad.component";

@Component({
  selector: "app-course-attendee-signature",
  templateUrl: "./course-attendee-signature.component.html",
  styleUrls: ["./course-attendee-signature.component.scss"],
})
export class CourseAttendeeSignatureComponent {
  @Input() speaker!: boolean;

  @Output() present = new EventEmitter<string>();

  @Output() absent = new EventEmitter<void>();

  @ViewChild("signaturePad")
  private signaturePad!: SignaturePadComponent;

  constructor() {}

  clear() {
    this.signaturePad.signaturePad?.clear();
  }

  onPresent() {
    if (!this.signaturePad.signaturePad?.isEmpty()) {
      const signature = this.signaturePad.signaturePad?.toDataURL("image/png");
      this.present.emit(signature);
    }
  }

  onAbsent() {
    this.absent.emit();
  }
}
