import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { map, startWith } from "rxjs";
import { Course } from "src/app/data/models/course.model";
import { SignaturePadComponent } from "../signature-pad/signature-pad.component";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-course-card-detail",
  templateUrl: "./course-card-detail.component.html",
  styleUrls: ["./course-card-detail.component.scss"],
})
export class CourseCardDetailComponent implements OnInit {
  form: FormGroup;
  public qrCodeUrl: string = "";
  public qrCodeWidth: number = 10;
  displayModal: boolean = false;

  constructor(
    fb: FormBuilder,
    private readonly onlineStatusService: OnlineStatusService
  ) {
    this.form = fb.group({
      title: ["", [Validators.required]],
      address: ["", [Validators.required]],
      start: ["", [Validators.required]],
      end: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.form.patchValue({
      ...this.course,
      start: new Date(this.course.start),
      end: new Date(this.course.end),
    });
    this.form.disable();
  }

  @Input() course!: Course;

  @Output() open = new EventEmitter<void>();

  @Output() close = new EventEmitter<string>();

  @ViewChild("signaturePadSpeaker")
  private signaturePad!: SignaturePadComponent;

  submit() {
    console.log("test");
  }

  formBlocked: boolean = true;
  qrCodeDisplay: boolean = false;
  endCourseDisplay: boolean = false;

  toggleForm(reset = false) {
    this.formBlocked = !this.formBlocked;
    if (this.formBlocked) {
      this.form.disable();
      if (reset) {
        this.form.patchValue({
          ...this.course,
          start: new Date(this.course.start),
          end: new Date(this.course.end),
        });
      }
    } else {
      this.form.enable();
    }
  }

  showQrCode() {
    var height = window.innerHeight;
    var width = window.innerWidth;
    // it's a square, take the smallest value to appear in full
    var size = height < width ? height : width;
    this.qrCodeWidth = (size * 60) / 100;

    this.qrCodeUrl =
      location.origin +
      "/courses/" +
      this.course.key +
      "/signature/qrcode/" +
      this.course.qrcode_key;
    this.displayModal = true;
    this.qrCodeDisplay = true;
  }

  showEndCourse() {
    this.endCourseDisplay = true;
  }

  errorSignature: string = "";

  endCourse() {
    if (!this.signaturePad.signaturePad?.isEmpty()) {
      this.errorSignature = "";
      const signature = this.signaturePad.signaturePad?.toDataURL("image/png");
      this.close.emit(signature);
    } else {
      this.errorSignature = "Merci de signer dans le cadre !";
    }

    this.endCourseDisplay = false;
  }

  isOnline() {
    return this.onlineStatusService.status.pipe(
      startWith(this.onlineStatusService.getStatus()),
      map((status) => status === OnlineStatusType.ONLINE)
    );
  }
}
