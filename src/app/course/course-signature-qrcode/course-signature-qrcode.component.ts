import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { map, Observable, startWith } from "rxjs";
import { Store, Select } from "@ngxs/store";
import { actionsExecuting } from "@ngxs-labs/actions-executing";

import { Course } from "src/app/data/models/course.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { GetCourse } from "src/app/data/actions/courses.actions";
import { CoursesState } from "src/app/data/states/courses.state";
import { Signature } from "src/app/data/models/signature.model";

import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import { UpdateSignature } from "src/app/data/actions/signatures.actions";

@Component({
  selector: "app-course-signature-qrcode",
  templateUrl: "./course-signature-qrcode.component.html",
  styleUrls: ["./course-signature-qrcode.component.scss"],
})
export class CourseSignatureQrcodeComponent implements OnInit {
  course$?: Observable<StrapiData<Course> | undefined>;
  displayModal: boolean = false;
  displayModalComponent = false;
  selectedSignature?: StrapiData<Signature>;

  @Select(actionsExecuting([GetCourse])) loadingCourse$!: Observable<boolean>;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly store: Store,
    private readonly onlineStatusService: OnlineStatusService
  ) {}

  ngOnInit() {
    registerLocaleData(localeFr, "fr");
    const key = this.route.snapshot.paramMap.get("key");
    const keyQr = this.route.snapshot.paramMap.get("keyQr");

    if (key && keyQr) {
      // Prepare observable.
      this.course$ = this.store
        .select(CoursesState.courses)
        .pipe(
          map((courses) =>
            courses.find(
              (course) =>
                course.attributes.key === key &&
                course.attributes.qrcode_key === keyQr
            )
          )
        );

      // Trigger fetch course in case it's unknown.
      this.store.dispatch(new GetCourse(key));
      return;
    }
  }

  onPresent(courseId: number, signature: string) {
    if (this.selectedSignature) {
      this.store
        .dispatch(
          new UpdateSignature(courseId, {
            id: this.selectedSignature.id,
            attributes: {
              present: true,
              date: new Date(),
              signature,
            },
          })
        )
        .subscribe(() => this.store.dispatch(new GetCourse(courseId)));
    }

    this.closeSignature();
  }

  get selectedAttendeeName() {
    const attendee =
      this.selectedSignature?.attributes?.attendee?.data?.attributes;
    return `${attendee?.firstname} ${attendee?.lastname.toUpperCase()}`;
  }

  openSignature(signature: StrapiData<Signature>) {
    this.selectedSignature = signature;
    this.displayModal = true;
  }

  closeSignature() {
    this.selectedSignature = undefined;
    this.displayModal = false;
  }

  isOnline() {
    return this.onlineStatusService.status.pipe(
      startWith(this.onlineStatusService.getStatus()),
      map((status) => status === OnlineStatusType.ONLINE)
    );
  }
}
