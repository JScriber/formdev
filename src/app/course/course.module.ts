import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FullCalendarModule } from "@fullcalendar/angular";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { CheckboxModule } from "primeng/checkbox";
import { TabViewModule } from "primeng/tabview";
import { TabMenuModule } from "primeng/tabmenu";

import { DataModule } from "../data/data.module";
import { CourseListComponent } from "./course-list/course-list.component";
import { CourseDetailComponent } from "./course-detail/course-detail.component";
import { CourseRoutingModule } from "./course-routing.module";
import { CourseCardLinkComponent } from "./components/course-card-link/course-card-link.component";
import { CourseCardAttendeeListComponent } from "./components/course-card-attendee-list/course-card-attendee-list.component";
import { CourseCardDetailComponent } from "./components/course-card-detail/course-card-detail.component";
import { SignaturePadComponent } from "./components/signature-pad/signature-pad.component";
import { CourseAttendeeSignatureComponent } from "./components/course-attendee-signature/course-attendee-signature.component";
import { AttendeeCardDetailComponent } from "./components/attendee-card-detail/attendee-card-detail.component";

import { ToolbarModule } from "primeng/toolbar";
import { ButtonModule } from "primeng/button";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { TableModule } from "primeng/table";
import { InputTextModule } from "primeng/inputtext";
import { CalendarModule } from "primeng/calendar";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { CardModule } from "primeng/card";
import { DialogModule } from "primeng/dialog";
import { BadgeModule } from "primeng/badge";
import { TooltipModule } from "primeng/tooltip";

import { CourseSignatureComponent } from "src/app/course/course-signature/course-signature.component";
import { QRCodeModule } from "angularx-qrcode";
import { CourseSignatureAttendeeComponent } from "src/app/course/course-signature-attendee/course-signature-attendee.component";
import { CourseSignatureQrcodeComponent } from "src/app/course/course-signature-qrcode/course-signature-qrcode.component";
import { CourseNotFoundComponent } from './components/course-not-found/course-not-found.component';
import { InvalidCodeComponent } from './components/invalid-code/invalid-code.component';

const PRIMENG_MODULES = [
  ToolbarModule,
  ButtonModule,
  ProgressSpinnerModule,
  TableModule,
  InputTextModule,
  CalendarModule,
  OverlayPanelModule,
  CardModule,
  BadgeModule,
  TooltipModule,
  DialogModule,
];

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin,
]);

@NgModule({
  imports: [
    CommonModule,
    CourseRoutingModule,
    DataModule,
    CardModule,
    ButtonModule,
    FullCalendarModule,
    CalendarModule,
    FormsModule,
    DialogModule,
    InputTextModule,
    CheckboxModule,
    TabViewModule,
    TabMenuModule,
    ToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    ...PRIMENG_MODULES,
    QRCodeModule,
  ],
  declarations: [
    CourseListComponent,
    CourseDetailComponent,
    CourseSignatureComponent,
    CourseSignatureQrcodeComponent,
    CourseSignatureAttendeeComponent,
    CourseCardLinkComponent,
    SignaturePadComponent,
    CourseAttendeeSignatureComponent,
    CourseCardAttendeeListComponent,
    CourseCardDetailComponent,
    AttendeeCardDetailComponent,
    CourseNotFoundComponent,
    InvalidCodeComponent,
  ],
})
export class CourseModule {}
