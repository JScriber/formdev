import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CourseSignatureAttendeeComponent } from "src/app/course/course-signature-attendee/course-signature-attendee.component";
import { CourseSignatureQrcodeComponent } from "src/app/course/course-signature-qrcode/course-signature-qrcode.component";
import { CourseSignatureComponent } from "src/app/course/course-signature/course-signature.component";
import { AuthenticationGuard } from "../security/authentication-guard.service";
import { SecurityModule } from "../security/security.module";
import { CourseDetailComponent } from "./course-detail/course-detail.component";
import { CourseListComponent } from "./course-list/course-list.component";

const routes: Routes = [
  {
    path: "",
    component: CourseListComponent,
    canActivate: [AuthenticationGuard],
  },
  {
    path: ":key/signature",
    component: CourseSignatureComponent,
  },
  {
    path: ":key/signature/qrcode/:keyQr",
    component: CourseSignatureQrcodeComponent,
  },
  {
    path: ":key/signature/:keySignature",
    component: CourseSignatureAttendeeComponent,
  },
  {
    path: ":id",
    component: CourseDetailComponent,
    canActivate: [AuthenticationGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SecurityModule],
  exports: [RouterModule],
})
export class CourseRoutingModule {}
