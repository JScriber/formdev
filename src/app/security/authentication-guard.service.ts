import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private readonly auth: AuthenticationService,
              private readonly router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.redirectToLogin();
      return false;
    }

    return true;
  }

  private redirectToLogin() {
    this.router.navigate(['login']);
  }
}
