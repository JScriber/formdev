import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JwtStorageService } from './jwt-storage.service';
import { AuthenticationService } from './authentication.service';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { AuthenticationGuard } from './authentication-guard.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    JwtStorageService,
    AuthenticationService,
    AuthenticationGuard,
    JwtInterceptor
  ]
})
export class SecurityModule { }
