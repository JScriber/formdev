import { Injectable } from '@angular/core';
import { JwtStorageService } from './jwt-storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {

  constructor(private readonly jwtStorage: JwtStorageService,
              private readonly jwtHelper: JwtHelperService,
              private readonly router: Router) {}

  authenticate(token: string) {
    this.jwtStorage.token = token;
  }

  public isAuthenticated(): boolean {
    const token = this.jwtStorage.token;

    if (!token) return false;

    return !this.jwtHelper.isTokenExpired(token);
  }

  public logout() {
    this.jwtStorage.token = null;
    this.router.navigate(['login']);
  }
}
