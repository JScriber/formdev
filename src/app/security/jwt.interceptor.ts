import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtStorageService } from './jwt-storage.service';

const AUTHENTICATION_URL = 'auth/local';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private readonly jwtStorage: JwtStorageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const token = this.jwtStorage.token;

      if (token && !request.url.includes(AUTHENTICATION_URL)) {
        // TODO: Enable following code if the API supports JWT tokens.
        // request = request.clone({
        //     setHeaders: { Authorization: `Bearer ${token}` }
        // });
      }

      return next.handle(request);
    }
}
