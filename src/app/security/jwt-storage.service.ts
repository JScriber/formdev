import { Injectable } from '@angular/core';

export const TOKEN_KEY = 'token';

@Injectable()
export class JwtStorageService {

  get token(): string | null {
    return localStorage.getItem(TOKEN_KEY);
  }

  set token(token: string | null) {
    if (token === null) {
      localStorage.removeItem(TOKEN_KEY);
    } else {
      localStorage.setItem(TOKEN_KEY, token);
    }
  }
}
