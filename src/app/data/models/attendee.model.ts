import { AttendeeComment } from "src/app/data/models/attendee-comment.model";
import { StrapiData } from "src/app/data/models/strapi/strapi-data";
import { StrapiResponse } from "src/app/data/models/strapi/strapi-response";

export interface Attendee {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  phone: string;
  attendee_comments: StrapiResponse<StrapiData<AttendeeComment>[], undefined>;
  createdAt: Date;
  publishedAt: Date;
  updatedAt: Date;
}
