export interface CourseComment {
  comment: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
}
