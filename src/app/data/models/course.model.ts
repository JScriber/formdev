import { CourseComment } from "src/app/data/models/course-comment.model";
import { Signature } from "./signature.model";
import { Speaker } from "./speaker.model";
import { StrapiData } from "./strapi/strapi-data";
import { StrapiResponse } from "./strapi/strapi-response";

export interface Course {
  title: string;
  address: string;
  start: Date;
  end: Date;
  signature_date: Date;
  signature: string;
  key: string;
  qrcode_key: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  signatures: StrapiResponse<StrapiData<Signature>[], undefined>;
  course_comments: StrapiResponse<StrapiData<CourseComment>[], undefined>;
  speakers: StrapiResponse<StrapiData<Speaker>[], undefined>;
}
