export interface AttendeeComment {
  comment: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
}
