import { Attendee } from "./attendee.model";
import { StrapiData } from "./strapi/strapi-data";
import { StrapiResponse } from "./strapi/strapi-response";

export interface Signature {
  present: boolean;
  key: string;
  signature: string;
  date: Date;
  date_limit: Date;

  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  attendee: StrapiResponse<StrapiData<Attendee>, undefined>;
}
