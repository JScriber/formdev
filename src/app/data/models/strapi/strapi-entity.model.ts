import { StrapiData } from "./strapi-data";
import { StrapiResponse } from "./strapi-response";

export interface StrapiEntity<D, M> extends StrapiResponse<StrapiData<D>, M> {}
  