export interface StrapiData<T> {
  id: number;
  attributes: T;
}
