import { StrapiData } from "./strapi-data";
import { StrapiResponse } from "./strapi-response";

interface PaginationMetadata {
  pagination: {
    page: number;
    pageSize: number;
    pageCount: number;
    total: number;
  };
}

export interface StrapiPagination<T> extends StrapiResponse<StrapiData<T>[], PaginationMetadata> {}
