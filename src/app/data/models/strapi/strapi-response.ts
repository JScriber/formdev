/**
 * Base generic Strapi response.
 */
export interface StrapiResponse<D, M> {
  data: D;
  meta: M;
}
