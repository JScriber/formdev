export interface Speaker {
    username: string;
    email: string;
    provider: string;
    firstname: string;
    lastname: string;
    phone: string;
}