import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from "./api.service";
import { Course } from "../models/course.model";
import { StrapiEntity } from "../models/strapi/strapi-entity.model";
import { StrapiPagination } from "../models/strapi/strapi-pagination.model";
import { HttpParams } from "@angular/common/http";
import { StrapiResponse } from "../models/strapi/strapi-response";
import { CourseComment } from "../models/course-comment.model";
import { StrapiData } from "../models/strapi/strapi-data";
import { AttendeeComment } from "../models/attendee-comment.model";
import { Attendee } from "../models/attendee.model";

@Injectable()
export class CourseService {
  constructor(private readonly api: ApiService) {}

  getCourse(id: number | string): Observable<StrapiEntity<Course, unknown>> {
    return this.api.getRequest("/courses/" + id);
  }

  getCourses(): Observable<StrapiPagination<Course>> {
    const params = new HttpParams().set("populate", "*");
    return this.api.getRequest("/courses", params);
  }

  closeCourses(id: number | string, signature: string, signature_date: Date): Observable<StrapiEntity<Course, unknown>> {
    const payload: Partial<Course> = {
      signature, signature_date
    };

    return this.api.putRequest("/courses/" + id, {
      data: payload
    });
  }

  addAttendeeComment(courseId: number | string, attendeeId: number | string, comment: string): Observable<StrapiResponse<StrapiData<AttendeeComment>[], undefined>> {
    return this.api.postRequest("/attendee-comments", {
      data: {
        course: courseId,
        attendee: attendeeId,
        speaker: 1, // TODO: Should be handled on the API side.
        comment
      }
    });
  }

  sendEmail(courseId: number | string): Observable<StrapiResponse<StrapiData<AttendeeComment>[], undefined>> {
    return this.api.getRequest("/courses/" + courseId + "/email");
  }

  sendEmailBySignature(signatureId: number | string): Observable<StrapiResponse<StrapiData<AttendeeComment>[], undefined>> {
    return this.api.getRequest("/signature/" + signatureId + "/email");
  }

  updateAttendee(attendeeId: number | string, attendee: Partial<Attendee>): Observable<StrapiResponse<StrapiData<Attendee>[], undefined>> {
    console.log(attendee);
    return this.api.putRequest("/attendees/" + attendeeId, {
      data: attendee
    });
  }

  removeAttendeeComment(commentId: number | string): Observable<void> {
    return this.api.deleteRequest("/attendee-comments/" + commentId);
  }

  addComment(courseId: number | string, comment: string): Observable<StrapiResponse<StrapiData<CourseComment>[], undefined>> {
    return this.api.postRequest("/course-comments", {
      data: {
        course: courseId,
        speaker: 1, // TODO: Should be handled on the API side.
        comment
      }
    });
  }

  removeComment(commentId: number | string): Observable<void> {
    return this.api.deleteRequest("/course-comments/" + commentId);
  }
}
