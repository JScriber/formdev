import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {

  constructor(private readonly httpClient: HttpClient) {}

  getRequest<T>(url: string, params?: HttpParams): Observable<T> {
    return this.httpClient.get<T>(environment.api + url, { params });
  }

  postRequest<T, P>(url: string, payload: P): Observable<T> {
    return this.httpClient.post<T>(environment.api + url, payload);
  }

  putRequest<T, P>(url: string, payload: P): Observable<T> {
    return this.httpClient.put<T>(environment.api + url, payload);
  }

  deleteRequest<T>(url: string): Observable<T> {
    return this.httpClient.delete<T>(environment.api + url);
  }
}
