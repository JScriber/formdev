import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { StrapiEntity } from '../models/strapi/strapi-entity.model';
import { Signature } from '../models/signature.model';
import { StrapiData } from '../models/strapi/strapi-data';

@Injectable()
export class SignatureService {

  constructor(private readonly api: ApiService) {}

  patchSignature(signature: StrapiData<Partial<Signature>>): Observable<StrapiEntity<Signature, unknown>> {
    return this.api.putRequest('/signatures/' + signature.id, {
      data: signature.attributes
    });
  }
}
