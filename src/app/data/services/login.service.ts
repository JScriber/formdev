import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

export interface Credentials {
  email: string;
  password: string;
}

export interface StrapiAuthResponse {
  jwt: string;
  user: {
    id: number;
    username: string;
  }
}

@Injectable()
export class LoginService {

  constructor(private readonly api: ApiService) {}

  login({ email, password }: Credentials): Observable<StrapiAuthResponse> {
    return this.api.postRequest("/auth/local", {
      identifier: email,
      password
    });
  }
}
