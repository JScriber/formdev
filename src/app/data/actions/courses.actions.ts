export class GetCourses {
  static readonly type = "[Courses] GetAll";
}

export class GetCourse {
  static readonly type = "[Courses] GetOne";

  constructor(public readonly id: number | string) {}
}

export class CloseCourse {
  static readonly type = "[Courses] CloseCourse";

  constructor(public readonly id: number | string,
              public readonly signature: string,
              public readonly date: Date) {}
}
