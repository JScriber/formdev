import { Signature } from "../models/signature.model";
import { StrapiData } from "../models/strapi/strapi-data";

export class UpdateSignature {
  static readonly type = '[Signatures] UpdateSignature';

  constructor(public readonly courseId: number,
              public readonly signature: StrapiData<Partial<Signature>>) {}
}

export class SyncSignatures {
  static readonly type = '[Signatures] SyncSignatures';
}
