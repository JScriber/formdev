import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { OnlineStatusModule } from 'ngx-online-status';
import { ApiService } from './services/api.service';
import { CourseService } from './services/course.service';
import { SignatureService } from './services/signature.service';
import { LoginService } from './services/login.service';
import { SecurityModule } from '../security/security.module';

// States.
import { CoursesState } from './states/courses.state';
import { SignaturesState } from './states/signatures.state';
import { JwtInterceptor } from '../security/jwt.interceptor';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SecurityModule,
    OnlineStatusModule,
    NgxsModule.forFeature([CoursesState, SignaturesState]),
  ],
  providers: [
    ApiService,
    CourseService,
    SignatureService,
    LoginService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ]
})
export class DataModule {}
