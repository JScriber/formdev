import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { OnlineStatusService, OnlineStatusType } from 'ngx-online-status';
import { catchError, EMPTY, of, tap, zip } from 'rxjs';
import { SyncSignatures, UpdateSignature } from '../actions/signatures.actions';
import { Signature } from '../models/signature.model';
import { StrapiData } from '../models/strapi/strapi-data';
import { SignatureService } from '../services/signature.service';

export interface PendingSignature {
  courseId: number;
  signature: StrapiData<Partial<Signature>>;
}

type SignatureStateModel = PendingSignature[];

@State<SignatureStateModel>({
  name: 'signatures',
  defaults: []
})
@Injectable()
export class SignaturesState {

  @Selector()
  static signatures(state: SignatureStateModel) {
    return state;
  }

  @Selector()
  static hasSignatures(state: SignatureStateModel) {
    return state.length > 0;
  }

  constructor(private readonly signatureService: SignatureService,
              private readonly onlineStatusService: OnlineStatusService) {}

  @Action(UpdateSignature)
  updateSignature(ctx: StateContext<SignatureStateModel>, { courseId, signature }: UpdateSignature) {
    const status = this.onlineStatusService.getStatus();
    if (status === OnlineStatusType.OFFLINE) {
      ctx.setState([ ...ctx.getState(), { courseId, signature }]);
      return of(ctx.getState());
    }

    return this.signatureService.patchSignature(signature);
  }

  @Action(SyncSignatures)
  syncSignatures(ctx: StateContext<SignatureStateModel>) {
    const status = this.onlineStatusService.getStatus();
    if (status === OnlineStatusType.OFFLINE) {
      return EMPTY
    }

    const signatures = [... ctx.getState()];
    console.log(`Sync of ${signatures.length} signatures.`);
    const requests = signatures.map(s => this.signatureService
      .patchSignature(s.signature)
      .pipe(catchError(() => EMPTY))
    );

    return zip(requests).pipe(
      tap(responses => {
        for (const response of responses) {
          const index = signatures.findIndex(s => s.signature.id === response.data.id);
          if (index !== -1) {
            signatures.splice(index, 1);
          }
        }

        ctx.setState(signatures);
      })
    );
  }
}
