import { Injectable } from "@angular/core";
import { State, Action, StateContext, Selector } from "@ngxs/store";
import { OnlineStatusService, OnlineStatusType } from "ngx-online-status";
import { EMPTY, tap } from "rxjs";
import { CloseCourse, GetCourse, GetCourses } from "../actions/courses.actions";
import { Course } from "../models/course.model";
import { StrapiData } from "../models/strapi/strapi-data";
import { CourseService } from "../services/course.service";

type CourseStateModel = StrapiData<Course>[];

@State<CourseStateModel>({
  name: "courses",
  defaults: [],
})
@Injectable()
export class CoursesState {
  @Selector()
  static courses(state: CourseStateModel) {
    return state;
  }

  constructor(
    private readonly courseService: CourseService,
    private readonly onlineStatusService: OnlineStatusService
  ) {}

  @Action(GetCourses)
  getCourses(ctx: StateContext<CourseStateModel>) {
    const status = this.onlineStatusService.getStatus();
    if (status === OnlineStatusType.OFFLINE) return EMPTY;

    return this.courseService
      .getCourses()
      .pipe(tap(({ data }) => ctx.setState(data)));
  }

  @Action(GetCourse)
  getCourse(ctx: StateContext<CourseStateModel>, { id }: GetCourse) {
    const status = this.onlineStatusService.getStatus();
    if (status === OnlineStatusType.OFFLINE) return EMPTY;
    
    return this.courseService.getCourse(id).pipe(
      tap(({ data }) => this.updateOrInsertCourse(ctx, data)),
    );
  }

  @Action(CloseCourse)
  closeCourse(ctx: StateContext<CourseStateModel>, { id, signature, date }: CloseCourse) {
    const status = this.onlineStatusService.getStatus();
    if (status === OnlineStatusType.OFFLINE) return EMPTY;
    
    return this.courseService.closeCourses(id, signature, date).pipe(
      tap(({ data }) => ctx.dispatch(new GetCourse(data.id))),
    );
  }

  private updateOrInsertCourse(ctx: StateContext<CourseStateModel>, course: StrapiData<Course>) {
    const courses = [... ctx.getState()];
    const localCourseIndex: number = courses.findIndex(c => c.id === course.id);

    if (localCourseIndex !== -1) {
      courses.splice(localCourseIndex, 1);
    }

    courses.push(course);
    ctx.setState(courses);
  }
}
