import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, tap } from 'rxjs';
import { AuthenticationService } from 'src/app/security/authentication.service';
import { LoginService, StrapiAuthResponse } from '../../data/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  form: FormGroup;

  loading = false;

  authError = false;

  showPassword = false;

  private subscription?: Subscription;

  constructor(private readonly router: Router,
              private readonly loginService: LoginService,
              private readonly authenticationService: AuthenticationService,
              fb: FormBuilder) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]] // TODO: Add password validator.
    })
  }

  submit() {
    if (this.form.valid) {
      if (this.subscription) this.subscription.unsubscribe();
      this.loading = true;

      this.subscription = this.loginService.login(this.form.value)
        .pipe(tap(() => this.loading = false))
        .subscribe((authResponse) => this.onAuthSuccess(authResponse), () => this.onAuthError());
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }

  private onAuthSuccess(authResponse: StrapiAuthResponse) {
    this.authenticationService.authenticate(authResponse.jwt);
    this.router.navigate(['/courses']);
  }

  private onAuthError() {
    this.authError = true;
  }
}
