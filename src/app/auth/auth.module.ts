import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./login/login.component";
import { AuthRoutingModule } from "./auth-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataModule } from "../data/data.module";

import { InputTextModule } from "primeng/inputtext";
import { CardModule } from "primeng/card";
import { PasswordModule } from "primeng/password";
import { ButtonModule } from "primeng/button";
import { SecurityModule } from "../security/security.module";

@NgModule({
  imports: [
    CommonModule,
    DataModule,
    SecurityModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    InputTextModule,
    CardModule,
    PasswordModule,
    ButtonModule,
  ],
  declarations: [LoginComponent],
})
export class AuthModule {}
