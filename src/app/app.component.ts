import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { OnlineStatusService, OnlineStatusType } from 'ngx-online-status';
import { filter, switchMap } from 'rxjs';
import { GetCourses } from './data/actions/courses.actions';
import { SyncSignatures } from './data/actions/signatures.actions';
import { SignaturesState } from './data/states/signatures.state';
import { AuthenticationService } from './security/authentication.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private readonly authenticationService: AuthenticationService,
              private readonly store: Store,
              private readonly onlineStatusService: OnlineStatusService,
              private readonly messageService: MessageService) {}

  ngOnInit(): void {
    this.onlineStatusService.status.subscribe(status => {
      switch (status) {
        case OnlineStatusType.OFFLINE:
          this.messageService.add({ severity: 'error', summary: 'Connexion perdue.' });
          break;
        case OnlineStatusType.ONLINE:
          this.messageService.add({ severity: 'success', summary: 'Connexion retrouvée !'});
          break;
      }
    });

    this.onlineStatusService.status.pipe(
      filter(status => status === OnlineStatusType.ONLINE),
      switchMap(() => this.store.select(SignaturesState.hasSignatures)),
      filter(hasSignatures => hasSignatures && this.authenticationService.isAuthenticated()),
      switchMap(() => this.store.dispatch(new SyncSignatures()))
    ).subscribe(() => this.store.dispatch(new GetCourses()));
  }

}
